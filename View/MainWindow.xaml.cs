﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using Control;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Handler handler;
        public MainWindow()
        {
            InitializeComponent();
            handler = Handler.GetInstance();
            RefreshComputersDataGrid();
            RefreshPhonesDataGrid();
            RefreshEmployeeDataGrid();
            RefreshMiscDataGrid();
            RefreshComputerMouseDataGrid();
            FillHardwareStatusComboBox();
        }

        #region Existing Methods
        public int GetCurrentEmployeeId()
        {
            DataRowView row = (DataRowView) DGridEmployees.SelectedItem;
            int employeeID = (int) row["MedarbejderId"];
            return employeeID;
        }
        public int GetCurrentComputerId()
        {
            DataRowView row = (DataRowView)DGridComputers.SelectedItem;
            int hardwareID = (int)row["HardwareId"];
            return hardwareID;
        }
        public int GetCurrentPhoneId()
        {
            DataRowView row = (DataRowView)DGridPhones.SelectedItem;
            int hardwareID = (int)row["HardwareId"];
            return hardwareID;
        }
        public int GetCurrentComputerMouseId()
        {
            DataRowView row = (DataRowView) DGridComputerMouse.SelectedItem;
            int hardwareID = (int)row["HardwareId"];
            return hardwareID;
        }
        public int GetCurrentMiscId()
        {
            DataRowView row = (DataRowView) DGridMisc.SelectedItem;
            int hardwareID = (int)row["HardwareId"];
            return hardwareID;
        }
        #endregion

        #region Employee Tab
        //Medarbejder-tab metoder starter her
        public void RefreshEmployeeDataGrid()
        {
            DGridEmployees.ItemsSource = handler.ViewAllEmployees().AsDataView();
        }
        public void GetEmployeeHardware(int employeeId)
        {
            DGridEmployeeHardware.ItemsSource = handler.GetHardwareForEmployee(employeeId).AsDataView();
        }
        private void DgridEmployeeSelect_Click(object sender, SelectionChangedEventArgs e)
        {
            if (DGridEmployees.SelectedItem != null)
            {
                GetEmployeeHardware(GetCurrentEmployeeId());
            }
        }
        private void ButtonAddEmployee_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddEmployeeWindow addEmployeeWindow = new AddEmployeeWindow();
                addEmployeeWindow.Owner = this;
                addEmployeeWindow.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Der opstod en fejl: " + ex.Message.ToString(), "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ButtonEditEmployee_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddEmployeeWindow addEmployeeWindow = new AddEmployeeWindow(GetCurrentEmployeeId());
                addEmployeeWindow.Owner = this;
                addEmployeeWindow.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Vælg en medarbejder.", "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ButtonDeleteEmployee_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DGridEmployees.SelectedItem == null)
                {
                    MessageBox.Show("Vælg venligst en medarbejder.", "Intet valgt", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                }
                else
                {
                    int currentEmployeeId = GetCurrentEmployeeId();
                    if (MessageBox.Show("Er du sikker på, at du vil slette den valgte medarbejder?", "Slet medarbejder", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        handler.DeleteEmployee(currentEmployeeId);
                    }
                    RefreshEmployeeDataGrid();
                    RefreshComputersDataGrid();
                    RefreshPhonesDataGrid();
                    RefreshMiscDataGrid();
                    RefreshComputerMouseDataGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Der er sket en fejl:\n " + ex.Message.ToString(), "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region Computer Tab
        //Computer-tab metoder starter her
        public void RefreshComputersDataGrid()
        {
            if (ComboBoxFilterComputer.SelectedValue == null)
            {
                DGridComputers.ItemsSource = handler.ViewAllComputers().AsDataView();
            }
            else
            {
                DGridComputers.ItemsSource = handler.GetComputersFilteredByStatus(Convert.ToInt32(ComboBoxFilterComputer.SelectedValue)).AsDataView();
            }
        }
        private void ButtonAddComputer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddComputerWindow windowAddComputer = new AddComputerWindow();
                windowAddComputer.Owner = this;
                windowAddComputer.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Der opstod en fejl: " + ex.Message.ToString(), "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ButtonEditComputer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddComputerWindow windowAddComputer = new AddComputerWindow(GetCurrentComputerId());
                windowAddComputer.Owner = this;
                windowAddComputer.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Vælg en computer.", "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
        private void ButtonDeleteComputer_Click(object sender, RoutedEventArgs e)
        {
            int currentHardwareId = GetCurrentComputerId();
            if (MessageBox.Show("Er du sikker på, at du vil slette den valgte computer?", "???", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                handler.DeleteComputer(currentHardwareId);   
            }            
            RefreshComputersDataGrid();
        }
        private void ComboBoxFilterComputer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshComputersDataGrid();
        }
        private void ButtonResetFilterComputer_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxFilterComputer.SelectedValue = null;
            RefreshComputersDataGrid();
        }
        #endregion

        #region Phone Tab
        //Computer-tab metoder starter her
        public void RefreshPhonesDataGrid()
        {
            if (ComboBoxFilterPhone.SelectedValue == null)
            {
                DGridPhones.ItemsSource = handler.ViewAllPhones().AsDataView();
            }
            else
            {
                DGridPhones.ItemsSource = handler.GetPhonesFilteredByStatus(Convert.ToInt32(ComboBoxFilterPhone.SelectedValue)).AsDataView();
            }
            
        }
        private void ButtonAddPhone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddPhoneWindow windowAddPhone = new AddPhoneWindow();
                windowAddPhone.Owner = this;
                windowAddPhone.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Der opstod en fejl: " + ex.Message.ToString(), "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ButtonEditPhone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddPhoneWindow windowAddPhone = new AddPhoneWindow( Convert.ToInt32(GetCurrentPhoneId()));
                windowAddPhone.Owner = this;
                windowAddPhone.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Vælg en telefon: " + ex.Message, "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void ButtonDeletePhone_Click(object sender, RoutedEventArgs e)
        {
            int currentHardwareId = GetCurrentPhoneId();
            if (MessageBox.Show("Er du sikker på, at du vil slette den valgte telefon?", "???", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                handler.DeletePhone(currentHardwareId);
            }
            RefreshPhonesDataGrid();
        }
        private void ComboBoxFilterPhone_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshPhonesDataGrid();
        }
        private void ButtonResetFilterPhone_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxFilterPhone.SelectedValue = null;
            RefreshPhonesDataGrid();
        }
        #endregion

        #region ComputerMouse tab
        public void RefreshComputerMouseDataGrid()
        {
            
            if (ComboBoxFilterComputerMouse.SelectedValue == null)
            {
                DGridComputerMouse.ItemsSource = handler.ViewAllComputerMouse().AsDataView();
            }
            else
            {
                DGridComputerMouse.ItemsSource = handler.GetComputerMouseFilteredByStatus(Convert.ToInt32(ComboBoxFilterComputerMouse.SelectedValue)).AsDataView();
            }
        }
        private void ButtonAddComputerMouse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddComputerMouseWindow windowsAddComputerMouse = new AddComputerMouseWindow();
                windowsAddComputerMouse.Owner = this;
                windowsAddComputerMouse.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Der opstod en fejl: " + ex.Message.ToString(), "Fejl", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }   
        }
        private void ButtonEditComputerMouse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddComputerMouseWindow windowsAddComputerMouse = new AddComputerMouseWindow(Convert.ToInt32(GetCurrentComputerMouseId()));
                windowsAddComputerMouse.Owner = this;
                windowsAddComputerMouse.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Vælg en mus: " + ex.Message, "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ButtonDeleteComputerMouse_Click(object sender, RoutedEventArgs e)
        {
            int currentHardwareId = GetCurrentComputerMouseId();
            if (MessageBox.Show("Er du sikker på, at du vil slette den valgte enhed?", "???", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                handler.DeleteComputerMouse(currentHardwareId);
            }
            RefreshComputerMouseDataGrid();
        }
        private void ComboBoxFilterComputerMouse_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshComputerMouseDataGrid();
        }
        private void ButtonResetFilterComputerMouse_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxFilterComputerMouse.SelectedValue = null;
            RefreshComputerMouseDataGrid();
        }
        #endregion

        #region Misc Tab
        //Misc-tab metoder starter her
        public void RefreshMiscDataGrid()
        {
            if (ComboBoxFilterMisc.SelectedValue == null)
            {
                DGridMisc.ItemsSource = handler.ViewAllMiscs().AsDataView();
            }
            else
            {
                DGridMisc.ItemsSource = handler.GetMiscFilteredByStatus(Convert.ToInt32(ComboBoxFilterMisc.SelectedValue)).AsDataView();
            }
            
        }
        private void ButtonAddMisc_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddMiscWindow windowAddMisc = new AddMiscWindow();
                windowAddMisc.Owner = this;
                windowAddMisc.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Der opstod en fejl: " + ex.Message.ToString(), "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ButtonEditMisc_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddMiscWindow windowAddMisc = new AddMiscWindow(Convert.ToInt32(GetCurrentMiscId()));
                windowAddMisc.Owner = this;
                windowAddMisc.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Vælg en enhed: " + ex.Message, "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void ButtonDeleteMisc_Click(object sender, RoutedEventArgs e)
        {
            int currentHardwareId = GetCurrentMiscId();
            if (MessageBox.Show("Er du sikker på, at du vil slette den valgte enhed?", "???", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                handler.DeleteMisc(currentHardwareId);
            }
            RefreshMiscDataGrid();
        }
        private void ComboBoxFilterMisc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshMiscDataGrid();
        }
        private void ButtonResetFilterMisc_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxFilterMisc.SelectedValue = null;
            RefreshMiscDataGrid();
        }
        #endregion

        #region General Methods
        private void FillHardwareStatusComboBox()
        {
            DataTable hardwareStatusTable = handler.GetHardwareStatus();

            ComboBoxFilterComputer.ItemsSource = hardwareStatusTable.DefaultView;
            ComboBoxFilterComputer.SelectedValuePath = "HardwareStatusId";
            ComboBoxFilterComputer.DisplayMemberPath = "HardwareStatus";

            ComboBoxFilterPhone.ItemsSource = hardwareStatusTable.DefaultView;
            ComboBoxFilterPhone.SelectedValuePath = "HardwareStatusId";
            ComboBoxFilterPhone.DisplayMemberPath = "HardwareStatus";

            ComboBoxFilterComputerMouse.ItemsSource = hardwareStatusTable.DefaultView;
            ComboBoxFilterComputerMouse.SelectedValuePath = "HardwareStatusId";
            ComboBoxFilterComputerMouse.DisplayMemberPath = "HardwareStatus";

            ComboBoxFilterMisc.ItemsSource = hardwareStatusTable.DefaultView;
            ComboBoxFilterMisc.SelectedValuePath = "HardwareStatusId";
            ComboBoxFilterMisc.DisplayMemberPath = "HardwareStatus";

        }
        #endregion

        
    }
}
