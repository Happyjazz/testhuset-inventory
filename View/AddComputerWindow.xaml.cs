﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Control;

namespace View
{
    /// <summary>
    /// Interaction logic for AddComputerWindow.xaml
    /// </summary>
    public partial class AddComputerWindow : Window
    {
        private readonly Handler handler;
        private DataTable _hardwareStatusTable;
        private DataTable _dealerTable;
        private DataTable _operatingSystemsTable;
        private DataTable _officeVersionsTable;
        private DataTable _officeLicenseTypesTable;
        private DataTable _hardwareModelsTable;
        private DataTable _employeesTable;

        private int currentHardwareId;
        private int currentHardwareType = 1;


        public AddComputerWindow()
        {
            InitializeComponent();
            handler = Handler.GetInstance();
            FillHardwareStatusComboBox();
            FillDealerComboBox();
            FillOperatingSystemsComboBox();
            FillOfficeVersionsCombobox();
            FillOfficeLicenseTypesComboBox();
            FillHardwareModelsComboBox();
            FillEmployeeComboBox();
            btnAddComputer.Visibility = Visibility.Visible;
            btnUpdateComputer.Visibility = Visibility.Collapsed;
        }

        public AddComputerWindow(int hardwareId)
        {
            InitializeComponent();
            handler = Handler.GetInstance();
            FillHardwareStatusComboBox();
            FillDealerComboBox();
            FillOperatingSystemsComboBox();
            FillOfficeVersionsCombobox();
            FillOfficeLicenseTypesComboBox();
            FillHardwareModelsComboBox();
            FillEmployeeComboBox();

            btnAddComputer.Visibility = Visibility.Collapsed;
            btnUpdateComputer.Visibility = Visibility.Visible;

            currentHardwareId = hardwareId;

            DataTable computerTable = handler.GetSingleComputer(hardwareId);


            txtBoxHardwareName.Text = (string) computerTable.Rows[0]["HardwareName"];
            comboBoxStatus.SelectedValue = (int) computerTable.Rows[0]["HardwareStatusId"];
            if (computerTable.Rows[0]["EmployeeId"].ToString() != "")
            {
                comboBoxEmployee.SelectedValue = (int) computerTable.Rows[0]["EmployeeId"];
            }
            datePickerDateOfChange.Text = computerTable.Rows[0]["DateOfChange"].ToString();
            datePickerDateOfPurchase.SelectedDate = (DateTime) computerTable.Rows[0]["DateOfPurchase"];
            datePickerEndOfWarranty.SelectedDate = (DateTime) computerTable.Rows[0]["EndOfWarranty"];
            comboBoxDealer.SelectedValue = (int) computerTable.Rows[0]["DealerId"];
            txtBoxNote.Text = (string) computerTable.Rows[0]["Note"];
            txtBoxSerialNumber.Text = (string) computerTable.Rows[0]["SerialNumber"];
            comboBoxOperatingSystem.SelectedValue = (int) computerTable.Rows[0]["OperatingSystemId"];
            comboBoxOfficeVersion.SelectedValue = (int) computerTable.Rows[0]["OfficeVersionId"];
            comboBoxOfficeLicenseType.SelectedValue = (int) computerTable.Rows[0]["OfficeLicenseTypeId"];
            comboBoxHardwareModel.SelectedValue = (int) computerTable.Rows[0]["HardwareModelId"];
            if ((int) comboBoxStatus.SelectedValue == 2)
            {
                comboBoxEmployee.IsEnabled = true;
            }

        }



        #region Fill methods

        private void FillHardwareStatusComboBox()
        {
            _hardwareStatusTable = handler.GetHardwareStatus();
            comboBoxStatus.ItemsSource = _hardwareStatusTable.DefaultView;
            comboBoxStatus.SelectedValuePath = "HardwareStatusId";
            comboBoxStatus.DisplayMemberPath = "HardwareStatus";
        }

        private void FillDealerComboBox()
        {
            _dealerTable = handler.GetDealers();
            comboBoxDealer.ItemsSource = _dealerTable.DefaultView;
            comboBoxDealer.SelectedValuePath = "DealerId";
            comboBoxDealer.DisplayMemberPath = "DealerName";
        }

        private void FillOperatingSystemsComboBox()
        {
            _operatingSystemsTable = handler.GetOperatingSystems();
            comboBoxOperatingSystem.ItemsSource = _operatingSystemsTable.DefaultView;
            comboBoxOperatingSystem.SelectedValuePath = "OperatingSystemId";
            comboBoxOperatingSystem.DisplayMemberPath = "OperatingSystem";
        }

        private void FillOfficeVersionsCombobox()
        {
            _officeVersionsTable = handler.GetOfficeVersions();
            comboBoxOfficeVersion.ItemsSource = _officeVersionsTable.DefaultView;
            comboBoxOfficeVersion.SelectedValuePath = "OfficeVersionId";
            comboBoxOfficeVersion.DisplayMemberPath = "OfficeVersion";
        }

        private void FillOfficeLicenseTypesComboBox()
        {
            _officeLicenseTypesTable = handler.GetOfficeLicenseTypes();
            comboBoxOfficeLicenseType.ItemsSource = _officeLicenseTypesTable.DefaultView;
            comboBoxOfficeLicenseType.SelectedValuePath = "OfficeLicenseTypeId";
            comboBoxOfficeLicenseType.DisplayMemberPath = "OfficeLicenseType";
        }

        private void FillHardwareModelsComboBox()
        {
            _hardwareModelsTable = handler.GetHardwareModels();
            comboBoxHardwareModel.ItemsSource = _hardwareModelsTable.DefaultView;
            comboBoxHardwareModel.SelectedValuePath = "HardwareModelId";
            comboBoxHardwareModel.DisplayMemberPath = "HardwareModel";
        }

        private void FillEmployeeComboBox()
        {
            _employeesTable = handler.ViewAllEmployees();
            comboBoxEmployee.ItemsSource = _employeesTable.DefaultView;
            comboBoxEmployee.SelectedValuePath = "MedarbejderID";
            comboBoxEmployee.DisplayMemberPath = "Navn";
            comboBoxEmployee.IsEnabled = false;
        }

        #endregion

        private void ButtonAddComputer_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToString(txtBoxHardwareName.Text) == string.Empty || comboBoxStatus.SelectedValue == null ||
                txtBoxSerialNumber.Text == string.Empty)
            {
                MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
            else
            {
                handler.AddComputer(
                    txtBoxHardwareName.Text,
                    1,
                    Convert.ToInt32(comboBoxHardwareModel.SelectedValue),
                    Convert.ToInt32(comboBoxStatus.SelectedValue),
                    Convert.ToInt32(comboBoxEmployee.SelectedValue),
                    DateTime.Now,
                    Convert.ToDateTime(datePickerDateOfPurchase.SelectedDate),
                    Convert.ToDateTime(datePickerEndOfWarranty.SelectedDate),
                    txtBoxNote.Text,
                    Convert.ToInt32(comboBoxDealer.SelectedValue),
                    txtBoxSerialNumber.Text,
                    Convert.ToInt32(comboBoxOperatingSystem.SelectedValue),
                    Convert.ToInt32(comboBoxOfficeVersion.SelectedValue),
                    Convert.ToInt32(comboBoxOfficeLicenseType.SelectedValue));

                var myObject = this.Owner as MainWindow;
                myObject.RefreshComputersDataGrid();
                this.Close();
            }
        }

        private void ButtonUpdateComputer_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToString(txtBoxHardwareName.Text) == string.Empty || comboBoxStatus.SelectedValue == null ||
                txtBoxSerialNumber.Text == string.Empty)
            {
                MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
            else
            {
                handler.UpdateComputer(
                    currentHardwareId,
                    txtBoxHardwareName.Text,
                    currentHardwareType,
                    Convert.ToInt32(comboBoxHardwareModel.SelectedValue),
                    Convert.ToInt32(comboBoxStatus.SelectedValue),
                    Convert.ToInt32(comboBoxEmployee.SelectedValue),
                    DateTime.Now,
                    Convert.ToDateTime(datePickerDateOfPurchase.SelectedDate),
                    Convert.ToDateTime(datePickerEndOfWarranty.SelectedDate),
                    txtBoxNote.Text,
                    Convert.ToInt32(comboBoxDealer.SelectedValue),
                    txtBoxSerialNumber.Text,
                    Convert.ToInt32(comboBoxOperatingSystem.SelectedValue),
                    Convert.ToInt32(comboBoxOfficeVersion.SelectedValue),
                    Convert.ToInt32(comboBoxOfficeLicenseType.SelectedValue)
                    );

                var myObject = Owner as MainWindow;
                myObject.RefreshComputersDataGrid();
                Close();
            }
        }

        private void ComboBoxStatus_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Convert.ToInt32(comboBoxStatus.SelectedValue) == 2)
            {
                comboBoxEmployee.IsEnabled = true;
            }
            else
            {
                comboBoxEmployee.IsEnabled = false;
                comboBoxEmployee.SelectedValue = null;
            }
        }
    }
}