﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Control;

namespace View
{
    /// <summary>
    /// Interaction logic for AddEmployeeWindow.xaml
    /// </summary>
    public partial class AddEmployeeWindow : Window
    {
        private readonly Handler handler;
        private int _currentEmployeeId;
        public AddEmployeeWindow()
        {
            InitializeComponent();
            handler = Handler.GetInstance();

            btnAddComputer.Visibility = Visibility.Visible;
            btnUpdateComputer.Visibility = Visibility.Collapsed;
        }

        public AddEmployeeWindow(int employeeId)
        {
            InitializeComponent();
            handler = Handler.GetInstance();

            btnAddComputer.Visibility = Visibility.Collapsed;
            btnUpdateComputer.Visibility = Visibility.Visible;

            _currentEmployeeId = employeeId;

            DataTable employeeTable = handler.GetSingleEmployee(employeeId);

            txtBoxEmployeeFirstName.Text = (string)employeeTable.Rows[0]["FirstName"];
            txtBoxEmployeeLastName.Text = (string)employeeTable.Rows[0]["LastName"];
            txtBoxEmployeeInitials.Text = (string)employeeTable.Rows[0]["Initials"];
            txtBoxDateCreated.Text = Convert.ToString(employeeTable.Rows[0]["DateCreated"]);
        }

        private void ButtonUpdateEmployee_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToString(txtBoxEmployeeFirstName) == string.Empty || Convert.ToString(txtBoxEmployeeLastName) == string.Empty || Convert.ToString(txtBoxEmployeeInitials) == string.Empty)
            {
                MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
            else
            {
                handler.UpdateEmployee(_currentEmployeeId, txtBoxEmployeeInitials.Text, txtBoxEmployeeFirstName.Text, txtBoxEmployeeLastName.Text);

                var myObject = this.Owner as MainWindow;
                myObject.RefreshEmployeeDataGrid();
                this.Close();
            }
        }

        private void ButtonAddEmployee_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToString(txtBoxEmployeeFirstName) == string.Empty || Convert.ToString(txtBoxEmployeeLastName) == string.Empty || Convert.ToString(txtBoxEmployeeInitials) == string.Empty)
            {
                MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
            else
            {
                handler.AddEmployee(txtBoxEmployeeInitials.Text, txtBoxEmployeeFirstName.Text, txtBoxEmployeeLastName.Text);

                var myObject = this.Owner as MainWindow;
                myObject.RefreshEmployeeDataGrid();
                this.Close();
            }
        }
    }
}
