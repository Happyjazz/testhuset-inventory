﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Control;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class TestWindow : Window
    {
        Handler handler;
        public TestWindow()
        {
            InitializeComponent();
            handler = Handler.GetInstance();
            dGrid.ItemsSource = handler.ViewAllComputers().AsDataView();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            string hardwareName = txtBoxHardwareName.Text;
            int hardwareType = Convert.ToInt32(txtBoxHardwareType.Text);
            int hardwareStatus = Convert.ToInt32(txtBoxStatus.Text);
            DateTime dateOfChange = Convert.ToDateTime(datePickerDateOfChange.SelectedDate);
            DateTime dateOfPurchase = Convert.ToDateTime(datePickerDateOfPurchase.SelectedDate);
            DateTime endOfWarrant = Convert.ToDateTime(datePickerEndOfWarranty.SelectedDate);
            int dealer = Convert.ToInt32(txtBoxDealer.Text);
            string note = txtBoxNote.Text;
            string serialNumber = txtBoxSerialNumber.Text;
            int operatingSystem = Convert.ToInt32(txtBoxOperatingSystem.Text);
            int officeVersion = Convert.ToInt32(txtBoxOfficeVersion.Text);
            int officeLicense = Convert.ToInt32(txtBoxOfficeLicense.Text);
            int hardwareModel = Convert.ToInt32(txtBoxComputerModel.Text);
            int employeeId = 1;
        

            try
            {
                handler.AddComputer(hardwareName, hardwareType, hardwareModel, hardwareStatus, employeeId, dateOfChange, dateOfPurchase, endOfWarrant, note,
                    dealer, serialNumber, operatingSystem, officeVersion, officeLicense);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor) e.PropertyDescriptor;
            e.Column.Header = propertyDescriptor.DisplayName;
            if (propertyDescriptor.DisplayName == "HardwareId")
            {
                e.Cancel = true;
            }
        }
    }
}
