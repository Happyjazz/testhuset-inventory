﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Control;

namespace View
{
    /// <summary>
    /// Interaction logic for AddComputerMouseWindow.xaml
    /// </summary>
    public partial class AddComputerMouseWindow : Window
    {
        private readonly Handler handler;
        private DataTable _hardwareStatusTable;
        private DataTable _dealerTable;
        private DataTable _hardwareModelsTable;
        private DataTable _employeesTable;

        private int currentHardwareId;
        private int currentHardwareType = 4;


        public AddComputerMouseWindow()
        {
            InitializeComponent();
            handler = Handler.GetInstance();
            FillHardwareStatusComboBox();
            FillDealerComboBox();
            FillHardwareModelsComboBox();
            FillEmployeeComboBox();
            btnAddComputerMouse.Visibility = Visibility.Visible;
            btnUpdateComputerMouse.Visibility = Visibility.Collapsed;
        }

        public AddComputerMouseWindow(int hardwareId)
        {
            InitializeComponent();
            handler = Handler.GetInstance();
            FillHardwareStatusComboBox();
            FillDealerComboBox();
            FillHardwareModelsComboBox();
            FillEmployeeComboBox();

            btnAddComputerMouse.Visibility = Visibility.Collapsed;
            btnUpdateComputerMouse.Visibility = Visibility.Visible;

            currentHardwareId = hardwareId;

            DataTable computerMouseTable = handler.GetSingleComputerMouse(hardwareId);


            txtBoxHardwareName.Text = (string)computerMouseTable.Rows[0]["HardwareName"];
            comboBoxStatus.SelectedValue = (int)computerMouseTable.Rows[0]["HardwareStatusId"];
            if (computerMouseTable.Rows[0]["EmployeeId"].ToString() != "")
            {
                comboBoxEmployee.SelectedValue = (int)computerMouseTable.Rows[0]["EmployeeId"];
            }
            datePickerDateOfChange.Text = computerMouseTable.Rows[0]["DateOfChange"].ToString();
            datePickerDateOfPurchase.SelectedDate = (DateTime)computerMouseTable.Rows[0]["DateOfPurchase"];
            datePickerEndOfWarranty.SelectedDate = (DateTime)computerMouseTable.Rows[0]["EndOfWarranty"];
            comboBoxDealer.SelectedValue = (int)computerMouseTable.Rows[0]["DealerId"];
            txtBoxNote.Text = (string)computerMouseTable.Rows[0]["Note"];
            comboBoxHardwareModel.SelectedValue = (int)computerMouseTable.Rows[0]["HardwareModelId"];
            if ((int)comboBoxStatus.SelectedValue == 2)
            {
                comboBoxEmployee.IsEnabled = true;
            }

        }



        #region Fill methods
        
        private void FillHardwareStatusComboBox()
        {
            _hardwareStatusTable = handler.GetHardwareStatus();
            comboBoxStatus.ItemsSource = _hardwareStatusTable.DefaultView;
            comboBoxStatus.SelectedValuePath = "HardwareStatusId";
            comboBoxStatus.DisplayMemberPath = "HardwareStatus";
        }

        private void FillDealerComboBox()
        {
            _dealerTable = handler.GetDealers();
            comboBoxDealer.ItemsSource = _dealerTable.DefaultView;
            comboBoxDealer.SelectedValuePath = "DealerId";
            comboBoxDealer.DisplayMemberPath = "DealerName";
        }

        private void FillHardwareModelsComboBox()
        {
            _hardwareModelsTable = handler.GetHardwareModels();
            comboBoxHardwareModel.ItemsSource = _hardwareModelsTable.DefaultView;
            comboBoxHardwareModel.SelectedValuePath = "HardwareModelId";
            comboBoxHardwareModel.DisplayMemberPath = "HardwareModel";
        }

        private void FillEmployeeComboBox()
        {
            _employeesTable = handler.ViewAllEmployees();
            comboBoxEmployee.ItemsSource = _employeesTable.DefaultView;
            comboBoxEmployee.SelectedValuePath = "MedarbejderID";
            comboBoxEmployee.DisplayMemberPath = "Navn";
            comboBoxEmployee.IsEnabled = false;
        }
        #endregion
        private void ButtonAddComputerMouse_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToString(txtBoxHardwareName.Text) == string.Empty || comboBoxStatus.SelectedValue == null)
            {
                MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
            else
            {
                handler.AddComputerMouse(
                    txtBoxHardwareName.Text,
                    currentHardwareType,
                    Convert.ToInt32(comboBoxHardwareModel.SelectedValue),
                    Convert.ToInt32(comboBoxStatus.SelectedValue),
                    Convert.ToInt32(comboBoxEmployee.SelectedValue),
                    DateTime.Now,
                    Convert.ToDateTime(datePickerDateOfPurchase.SelectedDate),
                    Convert.ToDateTime(datePickerEndOfWarranty.SelectedDate),
                    txtBoxNote.Text,
                    Convert.ToInt32(comboBoxDealer.SelectedValue));

                var myObject = this.Owner as MainWindow;
                myObject.RefreshComputerMouseDataGrid();
                this.Close();
            }
        }

        private void ButtonUpdateComputerMouse_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToString(txtBoxHardwareName.Text) == string.Empty || comboBoxStatus.SelectedValue == null)
            {
                MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
            else
            {
                handler.UpdateComputerMouse(
                currentHardwareId,
                txtBoxHardwareName.Text,
                currentHardwareType,
                Convert.ToInt32(comboBoxHardwareModel.SelectedValue),
                Convert.ToInt32(comboBoxStatus.SelectedValue),
                Convert.ToInt32(comboBoxEmployee.SelectedValue),
                DateTime.Now,
                Convert.ToDateTime(datePickerDateOfPurchase.SelectedDate),
                Convert.ToDateTime(datePickerEndOfWarranty.SelectedDate),
                txtBoxNote.Text,
                Convert.ToInt32(comboBoxDealer.SelectedValue));

                var myObject = Owner as MainWindow;
                myObject.RefreshComputerMouseDataGrid();
                Close();
            }
        }

        private void ComboBoxStatus_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Convert.ToInt32(comboBoxStatus.SelectedValue) == 2)
            {
                comboBoxEmployee.IsEnabled = true;
            }
            else
            {
                comboBoxEmployee.IsEnabled = false;
                comboBoxEmployee.SelectedValue = null;
            }
        }
    }
}