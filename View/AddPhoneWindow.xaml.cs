﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Control;

namespace View
{
    /// <summary>
    /// Interaction logic for AddPhoneWindow.xaml
    /// </summary>
    public partial class AddPhoneWindow : Window
    {
        private readonly Handler _handler;
        private DataTable _hardwareStatusTable;
        private DataTable _dealerTable;
        private DataTable _hardwareModelsTable;
        private DataTable _employeesTable;

        private readonly int _currentHardwareId;
        private const int CurrentHardwareType = 2;

        public AddPhoneWindow()
        {
            InitializeComponent();
            _handler = Handler.GetInstance();
            FillHardwareStatusComboBox();
            FillDealerComboBox();
            FillHardwareModelsComboBox();
            FillEmployeeComboBox();
            btnAddPhone.Visibility = Visibility.Visible;
            btnUpdatePhone.Visibility = Visibility.Collapsed;
        }

        public AddPhoneWindow(int hardwareId)
        {
            InitializeComponent();
            _handler = Handler.GetInstance();
            FillHardwareStatusComboBox();
            FillDealerComboBox();
            FillHardwareModelsComboBox();
            FillEmployeeComboBox();

            btnAddPhone.Visibility = Visibility.Collapsed;
            btnUpdatePhone.Visibility = Visibility.Visible;

            _currentHardwareId = hardwareId;

            DataTable phoneTable = _handler.GetSinglePhone(hardwareId);


            txtBoxHardwareName.Text = (string)phoneTable.Rows[0]["HardwareName"];
            comboBoxStatus.SelectedValue = (int)phoneTable.Rows[0]["HardwareStatusId"];
            if (phoneTable.Rows[0]["EmployeeId"].ToString() != "")
            {
                comboBoxEmployee.SelectedValue = (int)phoneTable.Rows[0]["EmployeeId"];
            }
            datePickerDateOfChange.Text = phoneTable.Rows[0]["DateOfChange"].ToString();
            datePickerDateOfPurchase.SelectedDate = (DateTime)phoneTable.Rows[0]["DateOfPurchase"];
            datePickerEndOfWarranty.SelectedDate = (DateTime)phoneTable.Rows[0]["EndOfWarranty"];
            comboBoxDealer.SelectedValue = (int)phoneTable.Rows[0]["DealerId"];
            txtBoxNote.Text = (string)phoneTable.Rows[0]["Note"];
            txtBoxSerialNumber.Text = (string)phoneTable.Rows[0]["SerialNumber"];
            txtBoxIMEI.Text = Convert.ToString(phoneTable.Rows[0]["IMEI"]);
            comboBoxHardwareModel.SelectedValue = (int)phoneTable.Rows[0]["HardwareModelId"];
            if ((int)comboBoxStatus.SelectedValue == 2)
            {
                comboBoxEmployee.IsEnabled = true;
            }
        }
        #region Fill methods

        private void FillHardwareStatusComboBox()
        {
            _hardwareStatusTable = _handler.GetHardwareStatus();
            comboBoxStatus.ItemsSource = _hardwareStatusTable.DefaultView;
            comboBoxStatus.SelectedValuePath = "HardwareStatusId";
            comboBoxStatus.DisplayMemberPath = "HardwareStatus";
        }

        private void FillDealerComboBox()
        {
            _dealerTable = _handler.GetDealers();
            comboBoxDealer.ItemsSource = _dealerTable.DefaultView;
            comboBoxDealer.SelectedValuePath = "DealerId";
            comboBoxDealer.DisplayMemberPath = "DealerName";
        }
        private void FillHardwareModelsComboBox()
        {
            _hardwareModelsTable = _handler.GetHardwareModels();
            comboBoxHardwareModel.ItemsSource = _hardwareModelsTable.DefaultView;
            comboBoxHardwareModel.SelectedValuePath = "HardwareModelId";
            comboBoxHardwareModel.DisplayMemberPath = "HardwareModel";
        }

        private void FillEmployeeComboBox()
        {
            _employeesTable = _handler.ViewAllEmployees();
            comboBoxEmployee.ItemsSource = _employeesTable.DefaultView;
            comboBoxEmployee.SelectedValuePath = "MedarbejderID";
            comboBoxEmployee.DisplayMemberPath = "Navn";
            comboBoxEmployee.IsEnabled = false;
        }
        #endregion

        private void ButtonAddPhone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToString(txtBoxHardwareName.Text) == string.Empty || comboBoxStatus.SelectedValue == null ||
                txtBoxSerialNumber.Text == string.Empty)
                {
                    MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                }
                else
                {
                    _handler.AddPhone(
                        txtBoxHardwareName.Text,
                        2,
                        Convert.ToInt32(comboBoxHardwareModel.SelectedValue),
                        Convert.ToInt32(comboBoxStatus.SelectedValue),
                        Convert.ToInt32(comboBoxEmployee.SelectedValue),
                        DateTime.Now,
                        Convert.ToDateTime(datePickerDateOfPurchase.SelectedDate),
                        Convert.ToDateTime(datePickerEndOfWarranty.SelectedDate),
                        txtBoxNote.Text,
                        Convert.ToInt32(comboBoxDealer.SelectedValue),
                        Convert.ToInt64(txtBoxIMEI.Text),
                        Convert.ToString(txtBoxSerialNumber.Text));

                    var myObject = this.Owner as MainWindow;
                    myObject.RefreshPhonesDataGrid();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        private void ButtonUpdatePhone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToString(txtBoxHardwareName.Text) == string.Empty || comboBoxStatus.SelectedValue == null || txtBoxIMEI.Text == string.Empty)
                {
                    MessageBox.Show("Sørg venligst for at alle felter med * er udfyldt.", "???", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                }
                else
                {
                    _handler.UpdatePhone(
                    _currentHardwareId,
                    txtBoxHardwareName.Text,
                    CurrentHardwareType,
                    Convert.ToInt32(comboBoxHardwareModel.SelectedValue),
                    Convert.ToInt32(comboBoxStatus.SelectedValue),
                    Convert.ToInt32(comboBoxEmployee.SelectedValue),
                    DateTime.Now,
                    Convert.ToDateTime(datePickerDateOfPurchase.SelectedDate),
                    Convert.ToDateTime(datePickerEndOfWarranty.SelectedDate),
                    txtBoxNote.Text,
                    Convert.ToInt32(comboBoxDealer.SelectedValue),
                    Convert.ToInt64(txtBoxIMEI.Text),
                    txtBoxSerialNumber.Text);

                    var myObject = Owner as MainWindow;
                    myObject.RefreshPhonesDataGrid();
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fejl", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        private void ComboBoxStatus_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Convert.ToInt32(comboBoxStatus.SelectedValue) == 2)
            {
                comboBoxEmployee.IsEnabled = true;
            }
            else
            {
                comboBoxEmployee.IsEnabled = false;
                comboBoxEmployee.SelectedValue = null;
            }
        }

        #region TextBox Input Validation

        private void ValidateInputNumeric(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }
        #endregion
    }
}
