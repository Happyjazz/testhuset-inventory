﻿using System;
using System.Data;
using Domain;


namespace Control

{
    public class Handler
    {
        private Catalog catalog;

        #region Singleton-Metoder
        //Sørger for at man kun kan lave et objekt/instance af "catalog"
        private static Handler handler;
 
        //Metode til at hente singleton-instancen af Catalog
        public static Handler GetInstance()
        {
            if (handler == null)
            {
                handler = new Handler();
            }
            return handler;
        }

        //Constructor
        private Handler()
        {
            catalog = Catalog.GetInstance();
        }

        #endregion

        #region Employee Methods
        public DataTable ViewAllEmployees()
        {
            return catalog.ViewAllEmployeesTable();
        }
        public DataTable GetSingleEmployee(int employeeId)
        {
            return catalog.GetSingleEmployee(employeeId);
        }
        public void AddEmployee(string initials, string firstName, string lastName)
        {
            Employee employee = new Employee(initials, firstName, lastName);
            catalog.AddEmployee(employee);
        }
        public void UpdateEmployee(int employeeId, string initials, string firstName, string lastName)
        {
            Employee employee = new Employee(employeeId);
            employee.FirstName = firstName;
            employee.LastName = lastName;
            employee.Initials = initials;

            catalog.UpdateEmployee(employee);
        }
        public void DeleteEmployee(int employeeId)
        {
            catalog.DeleteEmployee(employeeId);
        }
        public DataTable GetHardwareForEmployee(int employeeId)
        {
            return catalog.GetHardwareForEmployee(employeeId);
        }
        #endregion

        #region Computer Methods
        public DataTable ViewAllComputers()
        {
            return catalog.ViewComputers();
        }
        public DataTable GetSingleComputer(int hardwareId)
        {
            DataTable result = catalog.GetSingleComputerTable(hardwareId);
            return result;
        }        
        public void AddComputer(string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer, string serialNumber, int operatingSystem, int officeVersion, int officeLicense)
        {
            Computer computer = new Computer(hardwareName, hardwareType, hardwareModel, hardwareStatus, employeeId, dateOfChange, dateOfPurchase, endOfWarranty, note, dealer, serialNumber, operatingSystem, officeVersion, officeLicense);
            catalog.AddComputer(computer);
        }
        public void UpdateComputer(int hardwareId, string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId,DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer, string serialNumber, int operatingSystem,int officeVersion, int officeLicense)
        {
            var computer = new Computer(hardwareId);
            computer.HardwareName = hardwareName;
            computer.HardwareType = hardwareType;
            computer.HardwareModel = hardwareModel;
            computer.HardwareStatus = hardwareStatus;
            computer.EmployeeId = employeeId;
            computer.DateOfChange = dateOfChange;
            computer.DateOfPurchase = dateOfPurchase;
            computer.EndOfWarranty = endOfWarranty;
            computer.Note = note;
            computer.Dealer = dealer;
            computer.SerialNumber = serialNumber;
            computer.OperatingSystem = operatingSystem;
            computer.OfficeLicense = officeLicense;
            computer.OfficeVersion = officeVersion;
            catalog.UpdateComputer(computer);
        }
        public void DeleteComputer(int currentHardwareId)
                {
                    catalog.DeleteComputer(currentHardwareId);
                }
        public DataTable GetComputersFilteredByStatus(int statusId)
        {
            return catalog.GetComputersFilteredByStatus(statusId);
        }
        #endregion

        #region Phone Methods
        public DataTable ViewAllPhones()
        {
            return catalog.ViewPhones();
        }
        public DataTable GetSinglePhone(int hardwareId)
        {
            return catalog.GetSinglePhoneTable(hardwareId);
        }
        public void AddPhone(string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer, Int64 imei, string serialNumber)
        {
            var phone = new Phone(hardwareName, hardwareType, hardwareModel, hardwareStatus, employeeId, dateOfChange, dateOfPurchase, endOfWarranty, note, dealer, imei, serialNumber);
            catalog.AddPhone(phone);
        }
        public void UpdatePhone(int hardwareId, string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer, Int64 imei, string serialNumber)
        {
            var phone = new Phone(hardwareId);
            phone.HardwareName = hardwareName;
            phone.HardwareType = hardwareType;
            phone.HardwareModel = hardwareModel;
            phone.HardwareStatus = hardwareStatus;
            phone.EmployeeId = employeeId;
            phone.DateOfChange = dateOfChange;
            phone.DateOfPurchase = dateOfPurchase;
            phone.EndOfWarranty = endOfWarranty;
            phone.Note = note;
            phone.Dealer = dealer;
            phone.Imei = imei;
            phone.SerialNumber = serialNumber;

            catalog.UpdatePhone(phone);
        }
        public void DeletePhone(int currentHardwareId)
        {
            catalog.DeletePhone(currentHardwareId);
        }
        public DataTable GetPhonesFilteredByStatus(int statusId)
        {
            return catalog.GetPhonesFilteredByStatus(statusId);
        }
        #endregion

        #region Mouse Methods
        public DataTable ViewAllComputerMouse()
        {
            return catalog.ViewComputerMouse();
        }
        public DataTable GetSingleComputerMouse(int hardwareId)
        {
            DataTable result = catalog.GetSingleComputerMouseTable(hardwareId);
            return result;
        }
        public void AddComputerMouse(string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus,int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note,int dealer)
        {
            ComputerMouse computerMouse = new ComputerMouse(hardwareName, hardwareType, hardwareModel, hardwareStatus, employeeId, dateOfChange, dateOfPurchase, endOfWarranty, note, dealer);
            catalog.AddComputerMouse(computerMouse);
        }
        public void UpdateComputerMouse(int hardwareId, string hardwareName, int hardwareType, int hardwareModel,int hardwareStatus, int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty,string note, int dealer)
        {
            ComputerMouse computerMouse = new ComputerMouse(hardwareId);
            computerMouse.HardwareName = hardwareName;
            computerMouse.HardwareType = hardwareType;
            computerMouse.HardwareModel = hardwareModel;
            computerMouse.HardwareStatus = hardwareStatus;
            computerMouse.EmployeeId = employeeId;
            computerMouse.DateOfChange = dateOfChange;
            computerMouse.DateOfPurchase = dateOfPurchase;
            computerMouse.EndOfWarranty = endOfWarranty;
            computerMouse.Note = note;
            computerMouse.Dealer = dealer;

            catalog.UpdateComputerMouse(computerMouse);
        }
        public void DeleteComputerMouse(int currentHardwareId)
        {
            catalog.DeleteComputerMouse(currentHardwareId);
        }
        public DataTable GetComputerMouseFilteredByStatus(int statusId)
        {
            return catalog.GetComputerMouseFilteredByStatus(statusId);
        }
        #endregion

        #region Misc Methods
        public DataTable ViewAllMiscs()
        {
            return catalog.ViewMisc();
        }
        public DataTable GetSingleMisc(int hardwareId)
        {
            DataTable result = catalog.GetSingleMiscTable(hardwareId);
            return result;
        }
        public void AddMisc(string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId,DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer)
        {
            Misc misc = new Misc(hardwareName, hardwareType, hardwareModel, hardwareStatus, employeeId, dateOfChange, dateOfPurchase, endOfWarranty, note, dealer);
            catalog.AddMisc(misc);
        }
        public void UpdateMisc(int hardwareId, string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer)
        {
            Misc misc = new Misc(hardwareId);
            misc.HardwareName = hardwareName;
            misc.HardwareType = hardwareType;
            misc.HardwareModel = hardwareModel;
            misc.HardwareStatus = hardwareStatus;
            misc.EmployeeId = employeeId;
            misc.DateOfChange = dateOfChange;
            misc.DateOfPurchase = dateOfPurchase;
            misc.EndOfWarranty = endOfWarranty;
            misc.Note = note;
            misc.Dealer = dealer;

            catalog.UpdateMisc(misc);
        }
        public void DeleteMisc(int currentHardwareId)
        {
            catalog.DeleteMisc(currentHardwareId);
        }
        public DataTable GetMiscFilteredByStatus(int statusId)
        {
            return catalog.GetMiscFilteredByStatus(statusId);
        }
        #endregion

        #region Get Properties Methods
            #region General Properties
        public DataTable GetHardwareTypes()
        {
            return catalog.GetHardwareTypesTable();
        }
        public DataTable GetHardwareStatus()
        {
            return catalog.GetHardwareStatusTable();
        }
        public DataTable GetDealers()
        {
            return catalog.GetDealersTable();
        }
        public DataTable GetHardwareModels()
        {
            return catalog.GetHardwareModelsTable();
        }
        #endregion
            #region Computer Properties
        public DataTable GetOperatingSystems()
        {
            return catalog.GetOperatingSystemsTable();
        }
        public DataTable GetOfficeVersions()
        {
            return catalog.GetOfficeVersionsTable();
        }
        public DataTable GetOfficeLicenseTypes()
        {
            return catalog.GetOfficeLicenseTypesTable();
        }
        #endregion
        #endregion

        
    }
}
