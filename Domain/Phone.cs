﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
      
    public class Phone : Hardware
    {
        private Int64 _imei;
        private string _serialNumber;

        public string SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }

        public Int64 Imei
        {
            get { return _imei; }
            set { _imei = value; }
        }

        public Phone(
            string hardwareName, 
            int hardwareType, 
            int hardwareModel, 
            int hardwareStatus, 
            int employeeId, 
            DateTime dateOfChange, 
            DateTime dateOfPurchase, 
            DateTime endOfWarranty, 
            string note, 
            int dealer, 
            Int64 imei, 
            string serialNumber) 
            : base(hardwareName, hardwareType, hardwareModel, 
            hardwareStatus, employeeId, dateOfChange, dateOfPurchase, 
            endOfWarranty, note, dealer)
        {
            // Disse er nedarvet fra Hardware klassen
            HardwareName = hardwareName;
            HardwareType = hardwareType;
            HardwareModel = hardwareModel;
            HardwareStatus = hardwareStatus;
            EmployeeId = employeeId;
            DateOfChange = dateOfChange;
            DateOfPurchase = dateOfPurchase;
            EndOfWarranty = endOfWarranty;
            Note = note;
            Dealer = dealer;

            // Disse er kun tilegnet en computer
            Imei = imei;
            SerialNumber = serialNumber;
        }

        public Phone(int hardwareId) : base(hardwareId)
        {
            Catalog catalog = Catalog.GetInstance();

            DataTable phoneTable = catalog.GetSinglePhoneTable(hardwareId);

            HardwareId = (int) phoneTable.Rows[0]["hardwareId"];
            HardwareName = (string)phoneTable.Rows[0]["HardwareName"];
            HardwareType = (int)phoneTable.Rows[0]["HardwareTypeId"];
            HardwareModel = (int)phoneTable.Rows[0]["HardwareModelId"];
            HardwareStatus = (int)phoneTable.Rows[0]["HardwareStatusId"];
            if (phoneTable.Rows[0]["EmployeeId"] == DBNull.Value)
            {
                EmployeeId = 0;
            }
            else
            {
                EmployeeId = Convert.ToInt32(phoneTable.Rows[0]["EmployeeId"]);
            }
            DateOfChange = (DateTime)phoneTable.Rows[0]["DateOfChange"];
            DateOfPurchase = (DateTime)phoneTable.Rows[0]["DateOfPurchase"];
            EndOfWarranty = (DateTime)phoneTable.Rows[0]["EndOfWarranty"];
            Note = (string)phoneTable.Rows[0]["Note"];
            Dealer = (int)phoneTable.Rows[0]["DealerId"];

            Imei = (Int64) phoneTable.Rows[0]["IMEI"];
            SerialNumber = (string) phoneTable.Rows[0]["SerialNumber"];
        }
    }
}
