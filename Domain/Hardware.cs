﻿using System;
using System.Data;

namespace Domain

{
    public class Hardware
    {
        private int _hardwareId;
        private string _hardwareName;
        private int _hardwareType;
        private int _hardwareStatus;
        private int _employeeId;
        private DateTime _dateOfChange;
        private DateTime _dateOfPurchase;
        private DateTime _endOfWarranty;
        private string _note;
        private int _dealer;
        private int _hardwareModel;

        #region Properties
        public int HardwareId
        {
            get { return _hardwareId; }
            set { _hardwareId = value; }
        }
        public string HardwareName
        {
            get { return _hardwareName; }
            set { _hardwareName = value; }
        }
        public int HardwareType
        {
            get { return _hardwareType; }
            set { _hardwareType = value; }
        }
        public int HardwareStatus
        {
            get { return _hardwareStatus; }
            set { _hardwareStatus = value; }
        }
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }
        public DateTime DateOfChange
        {
            get { return _dateOfChange; }
            set { _dateOfChange = value; }
        }
        public DateTime DateOfPurchase
        {
            get { return _dateOfPurchase; }
            set { _dateOfPurchase = value; }
        }
        public DateTime EndOfWarranty
        {
            get { return _endOfWarranty; }
            set { _endOfWarranty = value; }
        }
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }
        public int Dealer
        {
            get { return _dealer; }
            set { _dealer = value; }
        }
        public int HardwareModel
        {
            get { return _hardwareModel; }
            set { _hardwareModel = value; }
        }
        #endregion

        public Hardware(string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer)
        {
            HardwareName = hardwareName;
            HardwareType = hardwareType;
            HardwareModel = hardwareModel;
            HardwareStatus = hardwareStatus;
            EmployeeId = employeeId;
            DateOfChange = dateOfChange;
            DateOfPurchase = dateOfPurchase;
            EndOfWarranty = endOfWarranty;
            Note = note;
            Dealer = dealer;
        }

        public Hardware(int hardwareId)
        {
            Catalog catalog = Catalog.GetInstance();

            DataTable hardwareTable = catalog.GetSingleHardwareTable(hardwareId);

            HardwareId = (int)hardwareTable.Rows[0]["HardwareId"];
            HardwareName = (string) hardwareTable.Rows[0]["HardwareName"];
            HardwareType = (int) hardwareTable.Rows[0]["HardwareTypeId"];
            HardwareModel = (int) hardwareTable.Rows[0]["HardwareModelId"];
            HardwareStatus = (int) hardwareTable.Rows[0]["HardwareStatusId"];
            if (hardwareTable.Rows[0]["EmployeeId"] == DBNull.Value)
            {
                EmployeeId = 0;
            }
            else
            {
                EmployeeId = Convert.ToInt32(hardwareTable.Rows[0]["EmployeeId"]);
            }
            DateOfChange = (DateTime) hardwareTable.Rows[0]["DateOfChange"];
            DateOfPurchase = (DateTime) hardwareTable.Rows[0]["DateOfPurchase"];
            EndOfWarranty = (DateTime) hardwareTable.Rows[0]["EndOfWarranty"];
            Note = (string) hardwareTable.Rows[0]["Note"];
            Dealer = (int) hardwareTable.Rows[0]["DealerId"];
        }
    }
}
