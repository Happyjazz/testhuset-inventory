﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Employee
    {
        private int _employeeId;
        private string _initials;
        private string _firstName;
        private string _lastName;
        private string _fullName;
        private DateTime _dateCreated;

        #region Properties
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }
        public string Initials
        {
            get { return _initials; }
            set { _initials = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string FullName
        {
            get { return _firstName + " " + _lastName; }
            set { _fullName = value; }
        }
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        #endregion

        public Employee(string initials, string firstName, string lastName)
        {
            _initials = initials;
            _firstName = firstName;
            _lastName = lastName;
        }
        public Employee(int employeeId)
        {
            Catalog catalog = Catalog.GetInstance();

            DataTable employeeTable = catalog.GetSingleEmployee(employeeId);

            _employeeId = Convert.ToInt32(employeeTable.Rows[0]["EmployeeId"]);
            _initials = (string)employeeTable.Rows[0]["Initials"];
            _firstName = (string)employeeTable.Rows[0]["FirstName"];
            _lastName = (string)employeeTable.Rows[0]["LastName"];
            _fullName = _firstName + " " + _lastName;
            _dateCreated = Convert.ToDateTime(employeeTable.Rows[0]["DateCreated"]);
        }

    }
}
