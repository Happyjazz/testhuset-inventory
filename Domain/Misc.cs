﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Misc : Hardware
        {
           
            public Misc(
                string hardwareName, 
                int hardwareType, 
                int hardwareModel, 
                int hardwareStatus, 
                int employeeId, 
                DateTime dateOfChange, 
                DateTime dateOfPurchase, 
                DateTime endOfWarranty, 
                string note, 
                int dealer)
                : base(hardwareName, hardwareType, hardwareModel, 
                hardwareStatus, employeeId, dateOfChange, dateOfPurchase, 
                endOfWarranty, note, dealer)
            {
                // Disse er nedarvet fra Hardware klassen
                HardwareName = hardwareName;
                HardwareType = hardwareType;
                HardwareModel = hardwareModel;
                HardwareStatus = hardwareStatus;
                EmployeeId = employeeId;
                DateOfChange = dateOfChange;
                DateOfPurchase = dateOfPurchase;
                EndOfWarranty = endOfWarranty;
                Note = note;
                Dealer = dealer;
            }

        public Misc(int hardwareId) : base(hardwareId)
        {
            Catalog catalog = Catalog.GetInstance();

            DataTable miscTable = catalog.GetSingleMiscTable(hardwareId);

            HardwareId = Convert.ToInt32(miscTable.Rows[0]["hardwareId"]);
            HardwareName = Convert.ToString(miscTable.Rows[0]["HardwareName"]);
            HardwareType = Convert.ToInt32(miscTable.Rows[0]["HardwareTypeId"]);
            HardwareModel = Convert.ToInt32(miscTable.Rows[0]["HardwareModelId"]);
            HardwareStatus = Convert.ToInt32(miscTable.Rows[0]["HardwareStatusId"]);
            if (miscTable.Rows[0]["EmployeeId"] == DBNull.Value)
            {
                EmployeeId = 0;
            }
            else
            {
                EmployeeId = Convert.ToInt32(miscTable.Rows[0]["EmployeeId"]);
            }
            DateOfChange = Convert.ToDateTime(miscTable.Rows[0]["DateOfChange"]);
            DateOfPurchase = Convert.ToDateTime(miscTable.Rows[0]["DateOfPurchase"]);
            EndOfWarranty = Convert.ToDateTime(miscTable.Rows[0]["EndOfWarranty"]);
            Note = Convert.ToString(miscTable.Rows[0]["Note"]);
            Dealer = Convert.ToInt32(miscTable.Rows[0]["DealerId"]);
        }
    }
}

