﻿using System;
using System.Data;

namespace Domain
{
    public class Computer : Hardware
    {
        private int _officeLicense;
        private int _officeVersion;
        private int _operatingSystem;
        private string _serialNumber;

        #region Properties

        public string SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }

        public int OperatingSystem
        {
            get { return _operatingSystem; }
            set { _operatingSystem = value; }
        }

        public int OfficeVersion
        {
            get { return _officeVersion; }
            set { _officeVersion = value; }
        }

        public int OfficeLicense
        {
            get { return _officeLicense; }
            set { _officeLicense = value; }
        }

        #endregion

        public Computer(string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId, DateTime dateOfChange,
            DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer,
            string serialNumber, int operatingSystem, int officeVersion, int officeLicense)
            : base(hardwareName, hardwareType, hardwareModel, hardwareStatus, employeeId, dateOfChange, dateOfPurchase, endOfWarranty, note, dealer)
        {
            // Nedarvede Hardware-Properties
            HardwareName = hardwareName;
            HardwareType = hardwareType;
            HardwareModel = hardwareModel;
            HardwareStatus = hardwareStatus;
            EmployeeId = employeeId;
            DateOfChange = dateOfChange;
            DateOfPurchase = dateOfPurchase;
            EndOfWarranty = endOfWarranty;
            Note = note;
            Dealer = dealer;

            // Computer-Properties
            SerialNumber = serialNumber;
            OperatingSystem = operatingSystem;
            OfficeVersion = officeVersion;
            OfficeLicense = officeLicense;
        }

        public Computer(int hardwareId) : base(hardwareId)
        {
            Catalog catalog = Catalog.GetInstance();

            DataTable computerTable = catalog.GetSingleComputerTable(hardwareId);

            // Nedarvede Hardware-Properties
            HardwareId = (int) computerTable.Rows[0]["hardwareId"];
            HardwareName = (string)computerTable.Rows[0]["HardwareName"];
            HardwareType = (int)computerTable.Rows[0]["HardwareTypeId"];
            HardwareModel = (int)computerTable.Rows[0]["HardwareModelId"];
            HardwareStatus = (int)computerTable.Rows[0]["HardwareStatusId"];
            if (computerTable.Rows[0]["EmployeeId"] == DBNull.Value)
            {
                EmployeeId = 0;
            }
            else
            {
                EmployeeId = Convert.ToInt32(computerTable.Rows[0]["EmployeeId"]);
            }
            DateOfChange = (DateTime)computerTable.Rows[0]["DateOfChange"];
            DateOfPurchase = (DateTime)computerTable.Rows[0]["DateOfPurchase"];
            EndOfWarranty = (DateTime)computerTable.Rows[0]["EndOfWarranty"];
            Note = (string)computerTable.Rows[0]["Note"];
            Dealer = (int)computerTable.Rows[0]["DealerId"];

            // Computer-Properties
            SerialNumber = (string) computerTable.Rows[0]["SerialNumber"];
            OperatingSystem = (int) computerTable.Rows[0]["OperatingSystemId"];
            OfficeVersion = (int) computerTable.Rows[0]["OfficeVersionId"];
            OfficeLicense = (int) computerTable.Rows[0]["OfficeLicenseTypeId"];
        }
    }
}