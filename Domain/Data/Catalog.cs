﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Domain
{
    public class Catalog
    {
        private const string ConnectionString =
            @"Data Source=tcp:home.happyjazz.eu,49172;Initial Catalog=Inventory;Integrated Security=False;User ID=inventorySA;Password=TestHuset2014;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";

        private readonly SqlConnection _con;

        #region Singleton-metoder

        //Sørger for at man kun kan lave et objekt/instance af "catalog"
        private static Catalog _catalog;

        //Metode til at hente singleton-instancen af Catalog

        //Constructor
        private Catalog()
        {
            _con = new SqlConnection(ConnectionString);
        }

        public static Catalog GetInstance()
        {
            if (_catalog == null)
            {
                _catalog = new Catalog();
            }
            return _catalog;
        }

        #endregion

        #region Properties Get-methods

            #region General properties
        public DataTable GetSingleHardwareTable(int hardwareId)
        {
            string query = @"
            SELECT
            *
            FROM dbo.Hardware
            WHERE Hardware.HardwareId = " + hardwareId;
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetHardwareTypesTable()
        {
            string query = @"SELECT * FROM dbo.HardwareTypes";
            DataTable result = queryGetDataTable(query);
            
            return result;
        }
        public DataTable GetHardwareStatusTable()
        {
            string query = @"SELECT * FROM dbo.HardwareStatus";
            
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetDealersTable()
        {
            string query = @"SELECT * FROM dbo.Dealers";

            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetHardwareModelsTable()
        {
            string query = @"
            SELECT HardwareModels.HardwareModelId, HardwareBrands.HardwareBrand + ' ' + HardwareModels.HardwareModel AS HardwareModel 
            FROM dbo.HardwareModels
            JOIN dbo.HardwareBrands ON HardwareBrands.HardwareBrandId = HardwareBrands.HardwareBrandId
            ";

            DataTable result = queryGetDataTable(query);
            return result;
        }
        #endregion

            #region Computer Properties
        public DataTable GetOperatingSystemsTable()
        {
            string query = @"SELECT * FROM dbo.OperatingSystems";

            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetOfficeVersionsTable()
        {
            string query = @"SELECT * FROM dbo.OfficeVersions";

            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetOfficeLicenseTypesTable()
        {
            string query = @"SELECT * FROM dbo.OfficeLicenseTypes";

            DataTable result = queryGetDataTable(query);
            return result;
        }
        #endregion

        #endregion

        #region Query Methods
        private DataTable queryGetDataTable(string query)
        {
            DataSet dataSet = new DataSet();
            DataTable result = null;
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dataSet);
                }
                result = dataSet.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
            return result;
        }
        #endregion

        #region Employee Methods
        public DataTable ViewAllEmployeesTable()
        {
            string query = @"SELECT initials AS Initialer, FirstName + ' ' + LastName AS Navn, DateCreated AS Oprettet, EmployeeId AS MedarbejderID FROM dbo.employees order by Initialer ASC";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetSingleEmployee(int employeeId)
        {
            string query = @"
            SELECT
            *
            FROM dbo.Employees
            WHERE Employees.EmployeeId = " + employeeId;
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public void AddEmployee(Employee employee)
        {
            string query = @"
            INSERT INTO dbo.Employees (Initials, FirstName, LastName) 
            VALUES (@Initials, @FirstName, @LastName)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@Initials", employee.Initials));
                    cmd.Parameters.Add(new SqlParameter("@FirstName", employee.FirstName));
                    cmd.Parameters.Add(new SqlParameter("@LastName", employee.LastName));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void UpdateEmployee(Employee employee)
        {
            string query = @"
            UPDATE dbo.Employees 
            SET Initials = @Initials,
            FirstName = @FirstName, 
            LastName = @LastName
            WHERE Employees.EmployeeId = @EmployeeId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", employee.EmployeeId));
                    cmd.Parameters.Add(new SqlParameter("@Initials", employee.Initials));
                    cmd.Parameters.Add(new SqlParameter("@FirstName", employee.FirstName));
                    cmd.Parameters.Add(new SqlParameter("@LastName", employee.LastName));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void DeleteEmployee(int employeeId)
        {
            string query = @"
            UPDATE Hardware SET EmployeeId = NULL, HardwareStatusId = 1 WHERE EmployeeId = @EmployeeId
            DELETE FROM Employees WHERE EmployeeId = @EmployeeId            
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", employeeId));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public DataTable GetHardwareForEmployee(int employeeId)
        {
            string query = @"
            SELECT 
            Hardware.HardwareName AS Hardwarenavn,
            HardwareTypes.HardwareType AS Hardwaretype,
            Hardware.DateOfChange AS 'Dato udleveret',
            HardwareBrands.HardwareBrand AS Mærke,
            HardwareModels.HardwareModel AS Model
            FROM Hardware
            JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            JOIN HardwareBrands ON HardwareBrands.HardwareBrandId = HardwareModels.HardwareBrandId
            WHERE Hardware.EmployeeId = " + employeeId + "ORDER BY Hardwaretype, 'Dato udleveret'";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        #endregion

        #region Computer Methods
        public DataTable ViewComputers()
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'PC Navn',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Dealers.DealerName AS 'Forhandler',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            AttributesComputers.SerialNumber AS 'Serienummer',
            OperatingSystems.OperatingSystem AS 'Operativ System',
            OfficeVersions.OfficeVersion AS 'Officeversion',
            OfficeLicenseTypes.OfficeLicenseType AS 'Office licenstype',
            --Hardware.Note AS 'Bemærkning',            
            Hardware.HardwareId
            FROM dbo.Hardware
            JOIN AttributesComputers ON AttributesComputers.HardwareId = Hardware.HardwareId
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            LEFT JOIN OperatingSystems ON OperatingSystems.OperatingSystemId = AttributesComputers.OperatingSystemId
            LEFT JOIN OfficeVersions ON OfficeVersions.OfficeVersionId = AttributesComputers.OfficeVersionId
            LEFT JOIN OfficeLicenseTypes ON OfficeLicenseTypes.OfficeLicenseTypeId = AttributesComputers.OfficeLicenseTypeId
            ORDER BY 'PC Navn'
            ";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetSingleComputerTable(int hardwareId)
        {
            string query = @"
            SELECT
            *
            FROM dbo.Hardware
            JOIN AttributesComputers ON AttributesComputers.HardwareId = Hardware.HardwareId
            WHERE Hardware.HardwareId = " + hardwareId;
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public void AddComputer(Computer computer)
        {
            string query = @"
            DECLARE @NewHardwareID INT

            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, EmployeeId, DealerId, HardwareModelId, 
            HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @EmployeeId, @DealerId, @HardwareModelId, @HardwareName, 
            @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            
            SELECT @NewHardwareID = SCOPE_IDENTITY()

            INSERT INTO dbo.AttributesComputers (HardwareId, OperatingSystemId, OfficeVersionId, 
            OfficeLicenseTypeId, SerialNumber) 
            VALUES (@NewHardwareID, @OperatingSystemId, @OfficeVersionId, @OfficeLicenseTypeId, @SerialNumber)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", computer.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", computer.HardwareStatus));
                    if (computer.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", computer.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", computer.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", computer.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", computer.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", computer.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", computer.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", computer.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", computer.Note));

                    cmd.Parameters.Add(new SqlParameter("@OperatingSystemId", computer.OperatingSystem));
                    cmd.Parameters.Add(new SqlParameter("@OfficeVersionId", computer.OfficeVersion));
                    cmd.Parameters.Add(new SqlParameter("@OfficeLicenseTypeId", computer.OfficeLicense));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", computer.SerialNumber));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void UpdateComputer(Computer computer)
        {
            string query = @"
            UPDATE dbo.Hardware 
            SET HardwareTypeId = @HardwareTypeId,
            HardwareStatusId = @HardwareStatusId, 
            EmployeeId = @EmployeeId,
            DealerId = @DealerId, 
            HardwareModelId = @HardwareModelId, 
            HardwareName = @HardwareName, 
            DateOfChange = @DateOfChange, 
            DateOfPurchase = @DateOfPurchase, 
            EndOfWarranty = @EndOfWarranty, 
            Note = @Note
            WHERE Hardware.HardwareId = @HardwareId
            
            UPDATE dbo.AttributesComputers 
            SET OperatingSystemId = @OperatingSystemId, 
            OfficeVersionId = @OfficeVersionId, 
            OfficeLicenseTypeId = @OfficeLicenseTypeId, 
            SerialNumber = @SerialNumber
            WHERE HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@HardwareId", computer.HardwareId));

                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", computer.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", computer.HardwareStatus));
                    if (computer.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", computer.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", computer.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", computer.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", computer.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", computer.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", computer.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", computer.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", computer.Note));

                    cmd.Parameters.Add(new SqlParameter("@OperatingSystemId", computer.OperatingSystem));
                    cmd.Parameters.Add(new SqlParameter("@OfficeVersionId", computer.OfficeVersion));
                    cmd.Parameters.Add(new SqlParameter("@OfficeLicenseTypeId", computer.OfficeLicense));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", computer.SerialNumber));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void DeleteComputer(int currentHardwareId)
        {
            string query = @"
            DELETE FROM AttributesComputers WHERE HardwareId = @HardwareId
            DELETE FROM Hardware WHERE HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareId", currentHardwareId));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public DataTable GetComputersFilteredByStatus(int statusId)
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'PC Navn',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Dealers.DealerName AS 'Forhandler',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            AttributesComputers.SerialNumber AS 'Serienummer',
            OperatingSystems.OperatingSystem AS 'Operativ System',
            OfficeVersions.OfficeVersion AS 'Officeversion',
            OfficeLicenseTypes.OfficeLicenseType AS 'Office licenstype',
            --Hardware.Note AS 'Bemærkning',            
            Hardware.HardwareId
            FROM dbo.Hardware
            JOIN AttributesComputers ON AttributesComputers.HardwareId = Hardware.HardwareId
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            LEFT JOIN OperatingSystems ON OperatingSystems.OperatingSystemId = AttributesComputers.OperatingSystemId
            LEFT JOIN OfficeVersions ON OfficeVersions.OfficeVersionId = AttributesComputers.OfficeVersionId
            LEFT JOIN OfficeLicenseTypes ON OfficeLicenseTypes.OfficeLicenseTypeId = AttributesComputers.OfficeLicenseTypeId
            WHERE Hardware.HardwareTypeId = 1 AND Hardware.HardwareStatusId = " + statusId + " ORDER BY 'PC Navn'";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        #endregion

        #region Phone Methods
        public DataTable ViewPhones()
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'Telefonnavn',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            AttributesPhone.IMEI,
            AttributesPhone.SerialNumber AS 'Serienummer',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            Dealers.DealerName AS 'Forhandler',
            Hardware.HardwareId
            FROM dbo.Hardware
            JOIN AttributesPhone ON AttributesPhone.HardwareId = Hardware.HardwareId
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            ORDER BY 'Telefonnavn'
            ";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetSinglePhoneTable(int hardwareId)
        {
            string query = @"
            SELECT
            *
            FROM dbo.Hardware
            JOIN AttributesPhone ON AttributesPhone.HardwareId = Hardware.HardwareId
			WHERE Hardware.HardwareId = " + hardwareId;
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public void AddPhone(Phone phone)
        {
            string query = @"
            DECLARE @NewHardwareID INT

            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, EmployeeId, DealerId, HardwareModelId, HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @EmployeeId, @DealerId, @HardwareModelId, @HardwareName, @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            
            SELECT @NewHardwareID = SCOPE_IDENTITY()

            INSERT INTO dbo.AttributesPhone (HardwareId, Imei, SerialNumber) 
            VALUES (@NewHardwareID, @Imei, @SerialNumber)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    
                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", phone.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", phone.HardwareStatus));
                    if (phone.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", phone.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", phone.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", phone.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", phone.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", phone.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", phone.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", phone.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", phone.Note));
                    cmd.Parameters.Add(new SqlParameter("@IMEI", phone.Imei));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", phone.SerialNumber));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void UpdatePhone(Phone phone)
        {
            string query = @"
            UPDATE dbo.Hardware 
            SET HardwareTypeId = @HardwareTypeId,
            HardwareStatusId = @HardwareStatusId, 
            EmployeeId = @EmployeeId,
            DealerId = @DealerId, 
            HardwareModelId = @HardwareModelId, 
            HardwareName = @HardwareName, 
            DateOfChange = @DateOfChange, 
            DateOfPurchase = @DateOfPurchase, 
            EndOfWarranty = @EndOfWarranty, 
            Note = @Note
            WHERE Hardware.HardwareId = @HardwareId
            
            UPDATE dbo.AttributesPhone 
            SET Imei = @Imei, 
            SerialNumber = @SerialNumber
            WHERE HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@HardwareId", phone.HardwareId));

                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", phone.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", phone.HardwareStatus));
                    if (phone.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", phone.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", phone.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", phone.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", phone.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", phone.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", phone.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", phone.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", phone.Note));

                    cmd.Parameters.Add(new SqlParameter("@Imei", phone.Imei));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", phone.SerialNumber));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void DeletePhone(int currentHardwareId)
        {
            string query = @"
            DELETE FROM AttributesPhone WHERE HardwareId = @HardwareId
            DELETE FROM Hardware WHERE HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareId", currentHardwareId));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public DataTable GetPhonesFilteredByStatus(int statusId)
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'Telefonnavn',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            AttributesPhone.IMEI,
            AttributesPhone.SerialNumber AS 'Serienummer',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            Dealers.DealerName AS 'Forhandler',
            Hardware.HardwareId
            FROM dbo.Hardware
            JOIN AttributesPhone ON AttributesPhone.HardwareId = Hardware.HardwareId
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            WHERE Hardware.HardwareTypeId = 2 AND Hardware.HardwareStatusId = " + statusId + " ORDER BY 'Telefonnavn'";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        #endregion

        #region Mouse Methods
        public DataTable ViewComputerMouse()
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'Navn på mus',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            Dealers.DealerName AS 'Forhandler',
            Hardware.HardwareId
            FROM dbo.Hardware
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            WHERE Hardware.HardwareTypeId = 4
            ORDER BY 'Navn på mus'
            ";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetSingleComputerMouseTable(int hardwareId)
        {
            string query = @"
            SELECT
            *
            FROM dbo.Hardware
            WHERE Hardware.HardwareId = " + hardwareId;

            DataTable result = queryGetDataTable(query);
            return result;
        }
        public void AddComputerMouse(ComputerMouse computerMouse)
        {
            string query = @"
            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, EmployeeId, DealerId, HardwareModelId, HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @EmployeeId, @DealerId, @HardwareModelId, @HardwareName, @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", computerMouse.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", computerMouse.HardwareStatus));
                    if (computerMouse.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", computerMouse.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", computerMouse.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", computerMouse.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", computerMouse.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", computerMouse.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", computerMouse.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", computerMouse.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", computerMouse.Note));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void UpdateComputerMouse(ComputerMouse computerMouse)
        {
            string query = @"
            UPDATE dbo.Hardware 
            SET HardwareTypeId = @HardwareTypeId,
            HardwareStatusId = @HardwareStatusId, 
            EmployeeId = @EmployeeId,
            DealerId = @DealerId, 
            HardwareModelId = @HardwareModelId, 
            HardwareName = @HardwareName, 
            DateOfChange = @DateOfChange, 
            DateOfPurchase = @DateOfPurchase, 
            EndOfWarranty = @EndOfWarranty, 
            Note = @Note
            WHERE Hardware.HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@HardwareId", computerMouse.HardwareId));
                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", computerMouse.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", computerMouse.HardwareStatus));
                    if (computerMouse.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", computerMouse.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", computerMouse.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", computerMouse.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", computerMouse.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", computerMouse.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", computerMouse.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", computerMouse.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", computerMouse.Note));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void DeleteComputerMouse(int currentHardwareId)
        {
            string query = @"
            DELETE FROM Hardware WHERE HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareId", currentHardwareId));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public DataTable GetComputerMouseFilteredByStatus(int statusId)
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'Navn på mus',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            Dealers.DealerName AS 'Forhandler',
            Hardware.HardwareId
            FROM dbo.Hardware
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            WHERE Hardware.HardwareTypeId = 4 AND Hardware.HardwareStatusId = " + statusId + " ORDER BY 'Navn på mus'";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        #endregion

        #region Misc Methods
        public DataTable ViewMisc()
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'Enhed navn',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            Dealers.DealerName AS 'Forhandler',
            Hardware.HardwareId
            FROM dbo.Hardware
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            WHERE Hardware.HardwareTypeId = 3
            ORDER BY 'Enhed navn'
            ";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        public DataTable GetSingleMiscTable(int hardwareId)
        {
            string query = @"
            SELECT
            *
            FROM dbo.Hardware
            WHERE Hardware.HardwareId = " + hardwareId;

            DataTable result = queryGetDataTable(query);
            return result;
        }
        public void AddMisc(Misc misc)  
        {
            string query = @"
            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, EmployeeId, DealerId, HardwareModelId, HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @EmployeeId, @DealerId, @HardwareModelId, @HardwareName, @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", misc.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", misc.HardwareStatus));
                    if (misc.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", misc.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", misc.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", misc.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", misc.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", misc.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", misc.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", misc.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", misc.Note));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void UpdateMisc(Misc misc)
        {
            string query = @"
            UPDATE dbo.Hardware 
            SET HardwareTypeId = @HardwareTypeId,
            HardwareStatusId = @HardwareStatusId, 
            EmployeeId = @EmployeeId,
            DealerId = @DealerId, 
            HardwareModelId = @HardwareModelId, 
            HardwareName = @HardwareName, 
            DateOfChange = @DateOfChange, 
            DateOfPurchase = @DateOfPurchase, 
            EndOfWarranty = @EndOfWarranty, 
            Note = @Note
            WHERE Hardware.HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@HardwareId", misc.HardwareId));
                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", misc.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", misc.HardwareStatus));
                    if (misc.EmployeeId == 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", misc.EmployeeId));
                    }
                    cmd.Parameters.Add(new SqlParameter("@DealerId", misc.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", misc.HardwareModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", misc.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", misc.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", misc.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", misc.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", misc.Note));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void DeleteMisc(int currentHardwareId)
        {
            string query = @"
            DELETE FROM Hardware WHERE HardwareId = @HardwareId
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareId", currentHardwareId));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public DataTable GetMiscFilteredByStatus(int statusId)
        {
            string query = @"
            SELECT
            Hardware.HardwareName AS 'Enhedsnavn',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Hardware.EndOfWarranty AS 'Garanti udløb',
            Dealers.DealerName AS 'Forhandler',
            Hardware.HardwareId
            FROM dbo.Hardware
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            WHERE Hardware.HardwareTypeId = 3 AND Hardware.HardwareStatusId = " + statusId + " ORDER BY 'Enhedsnavn'";
            DataTable result = queryGetDataTable(query);
            return result;
        }
        #endregion
    }
}