﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Domain
{
    public class Catalog
    {
        private const string ConnectionString =
            @"Data Source=tcp:home.happyjazz.eu,49172;Initial Catalog=Inventory;Integrated Security=False;User ID=inventorySA;Password=TestHuset2014;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";

        private readonly SqlConnection _con;

        #region Singleton-metoder

        //Sørger for at man kun kan lave et objekt/instance af "catalog"
        private static Catalog _catalog;

        //Metode til at hente singleton-instancen af Catalog

        //Constructor
        private Catalog()
        {
            _con = new SqlConnection(ConnectionString);
        }

        public static Catalog GetInstance()
        {
            if (_catalog == null)
            {
                _catalog = new Catalog();
            }
            return _catalog;
        }

        #endregion

        //Metode til at teste databaseforbindelsen
        public bool TestConnection()
        {
            bool canConnect = false;
            using (_con)
            {
                try
                {
                    _con.Open();
                    canConnect = true;
                }
                catch (SqlException ex)
                {
                }
                finally
                {
                    _con.Close();
                }
            }

            return canConnect;
        }
        #region Computer Methods
        #region AddComputer Method

        public void AddComputer(Computer computer)
        {
            string query = @"
            DECLARE @NewHardwareID INT

            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, DealerId, HardwareModelId, 
            HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @DealerId, @HardwareModelId, @HardwareName, 
            @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            
            SELECT @NewHardwareID = SCOPE_IDENTITY()

            INSERT INTO dbo.AttributesComputers (HardwareId, OperatingSystemId, OfficeVersionId, 
            OfficeLicenseTypeId, SerialNumber) 
            VALUES (@NewHardwareID, @OperatingSystemId, @OfficeVersionId, @OfficeLicenseTypeId, @SerialNumber)
            ";
            try
            {
                using (var cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    cmd.Parameters.Add(new SqlParameter("@HardwareTypeId", computer.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatusId", computer.HardwareStatus));
                    cmd.Parameters.Add(new SqlParameter("@DealerId", computer.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@HardwareModelId", computer.ComputerModel));
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", computer.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", computer.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", computer.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarranty", computer.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", computer.Note));

                    cmd.Parameters.Add(new SqlParameter("@OperatingSystemId", computer.OperatingSystem));
                    cmd.Parameters.Add(new SqlParameter("@OfficeVersionId", computer.OfficeVersion));
                    cmd.Parameters.Add(new SqlParameter("@OfficeLicenseTypeId", computer.OfficeLicense));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", computer.SerialNumber));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }

        #endregion

        #region ViewComputers Method
        public DataTable ViewComputers()
        {
            string query = @"
            SELECT
            Hardware.HardwareId,
            Hardware.HardwareName AS 'PC Navn',
            HardwareStatus.HardwareStatus AS 'Status',
            Hardware.DateOfChange AS 'Dato for ændring',
            Employees.Initials AS 'Medarbejder',
            HardwareBrands.HardwareBrand AS 'Mærke',
            HardwareModels.HardwareModel AS 'Model',
            Hardware.DateOfPurchase AS 'Indkøbsdato',
            Dealers.DealerName AS 'Forhandler',
            Hardware.EndOfWarranty AS 'Garanti Udløb',
            AttributesComputers.SerialNumber AS 'Serienummer',
            OperatingSystems.OperatingSystem AS 'Operativ System',
            OfficeVersions.OfficeVersion AS 'Officeversion',
            OfficeLicenseTypes.OfficeLicenseType AS 'Office licenstype',
            Hardware.Note AS 'Bemærkning'
            FROM dbo.Hardware
            JOIN AttributesComputers ON AttributesComputers.HardwareId = Hardware.HardwareId
            LEFT JOIN HardwareTypes ON HardwareTypes.HardwareTypeId = Hardware.HardwareTypeId
            LEFT JOIN HardwareStatus ON HardwareStatus.HardwareStatusId = Hardware.HardwareStatusId
            LEFT JOIN Employees ON Employees.EmployeeId = Hardware.EmployeeId
            LEFT JOIN Dealers ON Dealers.DealerId = Hardware.DealerId
            LEFT JOIN HardwareModels ON HardwareModels.HardwareModelId = Hardware.HardwareModelId
            LEFT JOIN HardwareBrands ON HardwareModels.HardwareBrandId = HardwareBrands.HardwareBrandId
            LEFT JOIN OperatingSystems ON OperatingSystems.OperatingSystemId = AttributesComputers.OperatingSystemId
            LEFT JOIN OfficeVersions ON OfficeVersions.OfficeVersionId = AttributesComputers.OfficeVersionId
            LEFT JOIN OfficeLicenseTypes ON OfficeLicenseTypes.OfficeLicenseTypeId = AttributesComputers.OfficeLicenseTypeId
            ";
            DataSet dataSet = new DataSet();
            DataTable result = null;
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dataSet);
                }
                result = dataSet.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
            return result;
        }
        #endregion
        #endregion
        #region AddPhone Method

        public void AddPhone(Phone phone)
        {
            string query = @"
            DECLARE @NewHardwareID INT

            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, DealerId, HardwareModelId, 
            HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @DealerId, @HardwareModelId, @HardwareName, 
            @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            
            SELECT @NewHardwareID = SCOPE_IDENTITY()

            INSERT INTO dbo.AttributesPhone (Imei, SerialNumber) 
            VALUES (@NewHardwareID, @Imei, @SerialNumber)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", phone.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@HardwareType", phone.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatus", phone.HardwareStatus));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", phone.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", phone.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarrant", phone.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", phone.Note));
                    cmd.Parameters.Add(new SqlParameter("@Dealer", phone.Dealer));
                    cmd.Parameters.Add(new SqlParameter("@Imei", phone.Imei));
                    cmd.Parameters.Add(new SqlParameter("@Dealer", phone.SerialNumber));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void AddMisc(Misc misc)
        {
            string query = @"
            DECLARE @NewHardwareID INT

            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, DealerId, HardwareModelId, 
            HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @DealerId, @HardwareModelId, @HardwareName, 
            @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            
            SELECT @NewHardwareID = SCOPE_IDENTITY()

            INSERT INTO dbo.AttributesPhone (Imei, SerialNumber) 
            VALUES (@NewHardwareID, @Imei, @SerialNumber)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", misc.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@HardwareType", misc.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatus", misc.HardwareStatus));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", misc.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", misc.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarrant", misc.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", misc.Note));
                    cmd.Parameters.Add(new SqlParameter("@Dealer", misc.Dealer));
                  

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }
        public void AddComputerMouse(ComputerMouse computerMouse)
        {
            string query = @"
            DECLARE @NewHardwareID INT

            INSERT INTO dbo.Hardware (HardwareTypeId, HardwareStatusId, DealerId, HardwareModelId, 
            HardwareName, DateOfChange, DateOfPurchase, EndOfWarranty, Note) 
            VALUES (@HardwareTypeId, @HardwareStatusId, @DealerId, @HardwareModelId, @HardwareName, 
            @DateOfChange, @DateOfPurchase, @EndOfWarranty, @Note)
            
            SELECT @NewHardwareID = SCOPE_IDENTITY()

            INSERT INTO dbo.AttributesPhone (Imei, SerialNumber) 
            VALUES (@NewHardwareID, @Imei, @SerialNumber)
            ";
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, _con))
                {
                    _con.Open();
                    cmd.Parameters.Add(new SqlParameter("@HardwareName", computerMouse.HardwareName));
                    cmd.Parameters.Add(new SqlParameter("@HardwareType", computerMouse.HardwareType));
                    cmd.Parameters.Add(new SqlParameter("@HardwareStatus", computerMouse.HardwareStatus));
                    cmd.Parameters.Add(new SqlParameter("@DateOfChange", computerMouse.DateOfChange));
                    cmd.Parameters.Add(new SqlParameter("@DateOfPurchase", computerMouse.DateOfPurchase));
                    cmd.Parameters.Add(new SqlParameter("@EndOfWarrant", computerMouse.EndOfWarranty));
                    cmd.Parameters.Add(new SqlParameter("@Note", computerMouse.Note));
                    cmd.Parameters.Add(new SqlParameter("@Dealer", computerMouse.Dealer));


                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _con.Close();
            }
        }

        #endregion
    }
}