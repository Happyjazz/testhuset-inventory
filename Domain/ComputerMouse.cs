﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ComputerMouse : Hardware
    {

        public ComputerMouse(string hardwareName, int hardwareType, int hardwareModel, int hardwareStatus, int employeeId, DateTime dateOfChange, DateTime dateOfPurchase, DateTime endOfWarranty, string note, int dealer)
            : base(hardwareName, hardwareType, hardwareModel, hardwareStatus, employeeId, dateOfChange, dateOfPurchase, endOfWarranty, note, dealer)
        {
            // Disse er nedarvet fra Hardware klassen
            HardwareName = hardwareName;
            HardwareType = hardwareType;
            HardwareModel = hardwareModel;
            HardwareStatus = hardwareStatus;
            EmployeeId = employeeId;
            DateOfChange = dateOfChange;
            DateOfPurchase = dateOfPurchase;
            EndOfWarranty = endOfWarranty;
            Note = note;
            Dealer = dealer;
        }

        public ComputerMouse(int hardwareId) : base(hardwareId)
        {
            Catalog catalog = Catalog.GetInstance();

            DataTable computerMouseTable = catalog.GetSingleComputerMouseTable(hardwareId);

            HardwareId = Convert.ToInt32(computerMouseTable.Rows[0]["hardwareId"]);
            HardwareName = Convert.ToString(computerMouseTable.Rows[0]["HardwareName"]);
            HardwareType = Convert.ToInt32(computerMouseTable.Rows[0]["HardwareTypeId"]);
            HardwareModel = Convert.ToInt32(computerMouseTable.Rows[0]["HardwareModelId"]);
            HardwareStatus = Convert.ToInt32(computerMouseTable.Rows[0]["HardwareStatusId"]);
            if (computerMouseTable.Rows[0]["EmployeeId"] == DBNull.Value)
            {
                EmployeeId = 0;
            }
            else
            {
                EmployeeId = Convert.ToInt32(computerMouseTable.Rows[0]["EmployeeId"]);
            }
            DateOfChange = Convert.ToDateTime(computerMouseTable.Rows[0]["DateOfChange"]);
            DateOfPurchase = Convert.ToDateTime(computerMouseTable.Rows[0]["DateOfPurchase"]);
            EndOfWarranty = Convert.ToDateTime(computerMouseTable.Rows[0]["EndOfWarranty"]);
            Note = Convert.ToString(computerMouseTable.Rows[0]["Note"]);
            Dealer = Convert.ToInt32(computerMouseTable.Rows[0]["DealerId"]);
        }
    }
}
